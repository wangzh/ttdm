export interface Item {
    uid: string;
    title: string;
    update_date?: Date;
    create_date: Date;
}

export interface ItemTask {
    uid: string;
    item_uid: string;
    todo_status: string;
    enable_status: boolean;
    create_date: Date;
}

export interface ItemRelation {
    uid: string;
    item_uid: string;
    parent_uid?: string;
    prev_uid?: string;
    next_uid?: string;
    create_date: Date;
    update_date: Date;
}

export interface ItemImage {
    uid: string;
    item_uid: string;
    image: Object;
}